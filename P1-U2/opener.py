#!/usr/bin/python3
# -*- coding: utf-8 -*-


# Se llaman e importan las librerías necesarias para la construcción
# del código

import pandas as pd


# Se obtienen todas las columnas para luego botar las que no serán tomadas
# en cuenta en el proyecto
def obtener_columnas(columnas):
    COLUMNAS = ["Country",
                "Alcoholic Beverages",
                "Animal Products",
                "Animal fats",
                "Aquatic Products, Other",
                "Cereals - Excluding Beer",
                "Eggs",
                "Fish, Seafood",
                "Fruits - Excluding Wine",
                "Meat",
                "Milk - Excluding Butter",
                "Miscellaneous",
                "Offals",
                "Oilcrops",
                "Pulses",
                "Spices",
                "Starchy Roots",
                "Stimulants",
                "Sugar Crops",
                "Sugar & Sweeteners",
                "Treenuts",
                "Vegetal Products",
                "Vegetable Oils",
                "Vegetables",
                "Obesity",
                "Undernourished",
                "Confirmed",
                "Deaths",
                "Recovered",
                "Active",
                "Population",
                "Unit (all except Population)"]

    # Lectura del archivo .csv
    data = pd.read_csv("./suministro_alimentos_kcal.csv")

    # Remoción de columnas sobrantes
    for i in columnas:
        COLUMNAS.remove(i)

    data.drop(COLUMNAS, axis=1, inplace=True)

    return data
