#!/usr/bin/python3
# -*- coding: utf-8 -*-


# Llamar a la(s) librería(s) necesaria(s)(Matpotlib, Pandas y Opener {Función
# propia}) y extraer funcionalidades a utilizarse en el desarrollo del
# programa.

import matplotlib.pyplot as plt
import opener
import pandas as pd


# Se construyen las funciones módulo por módulo y luego se añaden a una última
# función main:

#   1er Módulo: función de creación variable doble (Obesidad - Muertes)
def data_doble(data):
    doble = data[["Obesity", "Deaths"]].copy()
    doble = data[(data["Obesity"].notna()) & (data["Deaths"].notna())].copy()
    doble["Obesity"] = doble["Obesity"] / 100
    doble["Deaths"] = doble["Deaths"] / 100

    return doble


#   3er Módulo: Creación gráficos
def graphic_dos_obesity(doble):
    doble.plot(kind="bar", x="Country",
               subplots=True, figsize=(8, 9))
    plt.savefig("graphic_dos.png")


#   5er Módulo: Función principal
def main():
    new_data = opener.obtener_columnas(["Obesity",
                                        "Population",
                                        "Country",
                                        "Deaths"]
                                       )

    doble = data_doble(new_data)

    doble["Valor Real Obesidad"] = doble["Obesity"] * doble["Population"]
    doble.sort_values(by="Valor Real Obesidad", inplace=True, ascending=False)
    doble = doble.head(5)

    doble["Valor Real Muertos"] = doble["Deaths"] * doble["Population"]
    doble.sort_values(by="Valor Real Muertos", inplace=True, ascending=False)
    doble = doble.head(5)

    graphic_dos_obesity(doble)

    return doble
