#!/usr/bin/python3
# -*- coding: utf-8 -*-


# INTERFAZ GRÁFICA DEL PROYECTO DE PROGRAMACIÓN


# Se llaman e importan las librerías necesarias para la construcción
# del código

import aditional
import pandas
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
import H_Uno
import H_Dos
import H_Tres
import H_Cuatro
import H_Cinco


# Clases y funciones
class MainWindows():
    # Se define función de constructor
    def __init__(self):
        # Variable constructor
        self.builder = Gtk.Builder()
        # Se añade el archivo .ui desde la carpeta que lo aloja
        self.builder.add_from_file("./UI2.glade")

        self.window = self.builder.get_object("main_windows")
        # La ventana se designa como
        self.window.set_title("Análisis de dataset del USDA")
        # Al pulsar en el botón 'close' cierra completamente la actividad
        self.window.connect("destroy", Gtk.main_quit)
        # Se redimensiona el tamaño de la ventana
        self.window.set_default_size(800, 600)
        # Mostrar la ventana gráfica
        self.window.show_all()

        # Creación del combobox
        self.combobox = self.builder.get_object("combobox_hipotesis")
        self.hipotesis = Gtk.ListStore(str)
        # Rectángulo que puede contener texto
        cell = Gtk.CellRendererText()
        # Permite crear parámetros
        self.combobox.pack_start(cell, True)
        # Creación de opciones y parámetros del combobox
        self.combobox.add_attribute(cell, "text", 0)
        preguntas = ["Pregunta 1",
                     "Pregunta 2",
                     "Pregunta 3",
                     "Pregunta 4",
                     "Pregunta 5"]

        # Recorredor de las preguntas
        for pregunta in preguntas:
            self.hipotesis.append([pregunta])

        # Menú combobox
        self.combobox.set_model(model=self.hipotesis)
        self.combobox.connect("changed", self.seleccion)

        # Label donde se presentan las preguntas en la interfaz gráfica
        self.nombre_H = self.builder.get_object("hipotesis_x")
        self.grafico = self.builder.get_object("grafico")

        self.treeview = self.builder.get_object("datos_treeview")
        self.treeview.connect("cursor-changed", self.load_aditional)

        # Información adicional a la pregunta presentada
        self.info_adicional = self.builder.get_object("contexto_hipotesis")

        # Label donde se presenta la conclusión
        self.conclusion = self.builder.get_object("conclusion_hipotesis")

    # Iterador de las opciones-preguntas presentadas en el combobox
    def seleccion(self, btn=None):
        indice = self.combobox.get_active_iter()
        modelo = self.combobox.get_model()
        opcion = modelo[indice][0]
        self.nombre_H.set_text(f"escogi {opcion}")

        # Opción que presenta la hipótesis asociada a la pregunta 1
        if opcion == "Pregunta 1":
            self.nombre_H.set_text(f"¿Cuáles son los 5 paı́ses que tienen\n"
                                   "más habitantes con obesidad? Esta\n"
                                   "pregunta no es una cifra porcentual,sino\n"
                                   "que real, por lo que se debe realizar el\n"
                                   "cálculo a partir de la columna población\n"
                                   "(Population)\n"
                                   )

            # Muestra gráfico de la H_1 en la interfaz
            self.grafico.set_from_file("./graphic_uno.png")
            dataframe = H_Uno.main()

            # Conclusion H-1
            self.conclusion.set_text(f"Teniendo en cuenta que los cinco\n"
                                     "paises con más habitantes con\n"
                                     "obesidad (en relacion a la población)\n"
                                     "corresponden a USA, China, India,\n"
                                     "Brazil y Russia, se puede afirmar la\n"
                                     "congruencia de estos resultados al\n"
                                     "verlos desde una perspectiva\n"
                                     "poblacional, ya que, dichos paises\n"
                                     "se encuentran en el Top 10 de paises\n"
                                     "con mayor poblacion del mundo\n"
                                     "teniendo a China en primer lugar,\n"
                                     "India en segundo y Estados Unidos en\n"
                                     "tercer lugar. En otros puestos se\n"
                                     "encuentra Brazil y Russia\n"
                                     "respectivamente.\n"
                                     "(https://eacnur.org/blog/los-10-\n"
                                     "paises-mas-poblados-del-mundo-tc\n"
                                     "_alt45664n_o_pstn_o_pst/)\n"
                                     )

        # Opción que presenta la hipótesis asociada a la pregunta 2
        elif opcion == "Pregunta 2":
            self.nombre_H.set_text(f"¿Cuáles son los 5 paı́ses que tienen\n"
                                   "más habitantes con obesidad y que además\n"
                                   "los que más muertes tienen? Esta\n"
                                   "respuesta es distinta a la anterior,\n"
                                   "pues no necesariamente un paı́s que esté\n"
                                   "en el top5 de obesidad también tenga una\n"
                                   "mayor cantidad de muertes en cifras. Las\n"
                                   "cifras son en tipo entero, no porcentual\n"
                                   )

            # Muestra gráfico de la H-2 en la interfaz
            self.grafico.set_from_file("./graphic_dos.png")
            dataframe = H_Dos.main()

            # Conclusion H-2
            self.conclusion.set_text(f"Según los datos entregados en el\n"
                                     "dataframe USA, Brazil, India, México\n"
                                     "y UK son los paises con alta obesidad\n"
                                     "amen de un mayor indice de muertes\n"
                                     "de la mano. En cierto modo, no es\n"
                                     "impresionante de ver a causa de lo\n"
                                     "directamente proporcional entre\n"
                                     "muertes y habitantes (esto es, entre\n"
                                     "más personas hayan, mayor serán la\n"
                                     "cantidad de decesos) esto se aplica a\n"
                                     "Estados unidos (3°lugar), India\n"
                                     "(2°lugar), Brazil (6°lugar) y\n"
                                     "Mexico (10°lugar) en temas de\n"
                                     "población.\n"
                                     "(https://eacnur.org/blog/los-10-\n"
                                     "paises-mas-poblados-del-mundo-tc_\n"
                                     "alt45664n_o_pstn_o_pst/)\n"
                                     "Sin embargo, un punto de vista\n"
                                     "de gran importancia señala que, si\n"
                                     "se cumpliese el hecho anterior,\n"
                                     "tendriamos que Nigeria (7°lugar),\n"
                                     "Indonesia (4°lugar) o Pakinstan\n"
                                     "(5°lugar) y sobre todo China\n"
                                     "(1°lugar) deberian presentar altas\n"
                                     "tasas de mortalidad. Es por esto\n"
                                     "que si analizamos bien, la obesidad\n"
                                     "es un factor considerable a la hora\n"
                                     "de la mortalidad en ciertos\n"
                                     "contextos, por ejemplo, USA que\n"
                                     "lidera en obesidad pero no en\n"
                                     "poblacion,esta en la cuspide de\n"
                                     "(decesos-obesidad) probablemente\n"
                                     "por la interferencia de\n"
                                     "problemas cardiacos, diabetes,\n"
                                     "entre otros, caso similar ocurre\n"
                                     "con Brazil o México.\n"
                                     "Por lo tanto se puede deducir que\n"
                                     "la obesidad trae consigo un\n"
                                     "incremento -no menor- de decesos\n"
                                     "en ciertos paises\n"
                                     )

        # Opción que presenta la hipótesis asociada a la pregunta 3
        elif opcion == "Pregunta 3":
            self.nombre_H.set_text(f"¿Los paı́ses que tienen mayor consumo\n"
                                   "de alcohol son los paı́ses que más\n"
                                   "contagios confirmados tienen? graficar\n"
                                   "el Top10 de paı́ses más consumo de\n"
                                   "alcohol tienen vs más contagios activos\n"
                                   "tienen (scatter plot)\n"
                                   )

            # Muestra gráfico de la H-3 en la interfaz
            self.grafico.set_from_file("./grafico_Tres.png")
            dataframe = H_Tres.main()

            # Conclusión H-3
            self.conclusion.set_text(f"De los datos rescatados, podemos\n"
                                     "señalar que en algunos casos se\n"
                                     "cumple la condicion de <entre mayor\n"
                                     "consumo de alcohol mayor contagios>\n"
                                     "como ocurre con Czechia y Luxemborgo\n"
                                     "quienes son los paises\n"
                                     "que entran tanto en el Top 10 de\n"
                                     "contagios como del alcohol, sin\n"
                                     "embargo, podrian considerarse"
                                     "excepciones a causa de países\n"
                                     "lideres en el Top 10 de consumidores\n"
                                     "de alcohol, como lo son Belarus y\n"
                                     "Bosnia and Herzegovina, los cuales\n"
                                     "no se asoman al Top10 de casos\n"
                                     "confirmados. Viceversa con Armenia\n"
                                     "que lidera las tasas de contagios\n"
                                     "que de igual forma no forma parte del\n"
                                     "Top10 de países con altos índices\n"
                                     "de consumo de alcohol\n")

        # Opción que presenta la hipótesis asociada a la pregunta 4
        elif opcion == "Pregunta 4":
            self.nombre_H.set_text(f"¿Qué aspecto tienen en común paises\n"
                                   "con porcentajes altos\n"
                                   "de desnutrición y cuales aspectos\n"
                                   "en paises que tiene menos\n"
                                   )

            # Muestra gráfico de la H-4 en la interfaz
            self.grafico.set_from_file("./Desnutridos.png")
            dataframe = H_Cuatro.main()

            # Conclusión
            self.conclusion.set_text(f"Considerando los datos mostrados en\n"
                                     "la data, podemos definir por como el\n"
                                     "maximo exponente e la desnutricion a\n"
                                     "<Central African Republic> lo cual\n"
                                     "puede ser explicado por las\n"
                                     "adversidades que han presentado,\n"
                                     "especificamente por temas\n"
                                     "politicos/economicos.\n"
                                     "(https://reliefweb.int/report/central\n"
                                     "-african-republic/rep%C3%BAblica-\n"
                                     "centroafricana-la-desnutrici%C3%B3n\n"
                                     "-supera-con-creces-los)\n"
                                     "Por otro lado los paises con menor\n"
                                     "desnutricion,lo adquieren Australia,\n"
                                     "Austria, Islandia, Alemania,\n"
                                     "Uruguay, España, entre otros.\n"
                                     "Estos paises tienen en común\n"
                                     "cierta estabilidad politica y\n"
                                     "economica siendo, por lo general,\n"
                                     "de primer mundo. (Nótese que los\n"
                                     "datos de estos paises son una\n"
                                     "estimacion de los paises con\n"
                                     "indices de desnutricion => 2.0)\n"
                                     "Por lo tento, podemos decir que\n"
                                     "paises con equilibrio politico y\n"
                                     "economico tienden a tener bajos\n"
                                     "indices de desnutricion, cosa\n"
                                     "contraria pasa con paises\n"
                                     "inestables.\n")

        # Opción que presenta la hipótesis asociada a la pregunta 5
        elif opcion == "Pregunta 5":
            self.nombre_H.set_text(f"Comparar datos de COVID para paises con\n"
                                   "mayor probabilidad de estabilidad\n"
                                   "economica ante COVID de America del Sur\n"
                                   )

            # Muestra gráfico de la H-5 en la interfaz
            self.grafico.set_from_file("./grafico_cinco.png")
            dataframe = H_Cinco.main()

            # Conclusión H-5
            self.conclusion.set_text(f"Segun un reporte expuesto por la bbc\n"
                                     "(https://www.bbc.com/mundo/noticias-\n"
                                     "53452148) Colombia, Chile, Peru,\n"
                                     "Paraguay y Uruguay (jerarquicamente)\n"
                                     "son los paises con mayor probabilidad\n"
                                     "de estabilizarse economicamente de\n"
                                     "la pandemia. Analizando los datos\n"
                                     "con respecto al COVID de estos paises\n"
                                     "(Casos confirmados, decesos,\n"
                                     "recuperados y activos) se puede dar\n"
                                     "cuenta que mantienen en\n"
                                     "común un alto indice de recuperación\n"
                                     "amen de un bajo indice de\n"
                                     "mortandad. No obstante, los casos\n"
                                     "activos para paises como Paraguay y\n"
                                     "Colombia aún son altos, además, en\n"
                                     "Chile no hay una estimacion explícita.\n"
                                     "Por lo tanto, no hay una correlación\n"
                                     "clara entre datas de COVID y la\n"
                                     "situación económica, salvó por los\n"
                                     "grandes índices de recuperación.\n"
                                     )

        # Presenta las tablas asociadas a las hipótesis correspondientes
        if len(self.treeview.get_columns()) != 0:
            for col in self.treeview.get_columns():
                self.treeview.remove_column(col)

        # Conversión de tipo de las tablas
        n_columnas = len(dataframe.columns)
        self.lista_valores = Gtk.ListStore(*(n_columnas * [str]))
        cell = Gtk.CellRendererText()

        for columna in range(n_columnas):
            col = Gtk.TreeViewColumn(
                dataframe.columns[columna], cell, text=columna)
            self.treeview.append_column(col)

        self.treeview.set_model(model=self.lista_valores)

        for value in dataframe.values:
            fila = [str(linea) for linea in value]
            self.lista_valores.append(fila)

    # Modúlo que presentará la información adicional a la pregunta
    def load_aditional(self, btn=None):
        model, index = self.treeview.get_selection().get_selected()

        if model is None or index is None:
            return

        # Data asociada a cada país de manera independiente
        pais = model.get_value(index, 0)
        aditional_data = aditional.get_aditional_data(pais)
        texto = "Datos adicionales sobre el pais: \n"

        for data in aditional_data:
            texto += data + ": " + str(aditional_data[data]) + "\n"

        self.info_adicional.set_text(texto)


class acerca_de():
    # Constructor
    def __init__(self):
        # Señala el archivo glade (UI2.glade)
        self.builder = Gtk.Builder()
        self.builder.add_from_file("UI2.glade")

        # Abre
        self.emergente = self.builder.get_object("emergente")
        self.emergente.show_all()

        # Interaccion con el único botón: "Aceptar"
        self.btn_aceptar = self.builder.get_object("aceptar")
        self.btn_aceptar.connect("clicked", self.ok_press)

    # Cerrar
    def ok_press(self, btn=None):
        self.emergente.destroy()


# Llamado a ventana principal y ejecución
if __name__ == "__main__":
    MainWindows()
    acerca_de()
    Gtk.main()
